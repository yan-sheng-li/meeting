package com.lys.meeting.view.user;

import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import com.lys.meeting.dao.RoomDao;
import com.lys.meeting.model.Room;
import com.lys.meeting.utils.DataUtils;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class Reserve extends JPanel {

	private JTextField jt;
	private JTable table;
	RoomDao roomDao=new RoomDao();
	String header[]={"序号","会议室名称","地址","座位数量","状态"};

	/**
	 * Create the panel.
	 */
	public Reserve(int id) {
		setBorder(new TitledBorder(null, "\u9884\u7EA6", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		setLayout(null);
		
		JLabel lblNewLabel = new JLabel("\u8BF7\u8F93\u5165\u4F1A\u8BAE\u5BA4\u7684ID\uFF1A");
		lblNewLabel.setFont(new Font("宋体", Font.PLAIN, 16));
		lblNewLabel.setBounds(28, 26, 233, 21);
		add(lblNewLabel);
		
		jt = new JTextField();
		jt.setBounds(28, 57, 241, 21);
		add(jt);
		jt.setColumns(10);
		
		JButton jb = new JButton("\u6211\u8981\u9884\u7EA6");
		jb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/**
				 * 1.查询会议室是否存在
				 * 2.添加记录到申请表
				 */
				int roomId=Integer.parseInt(jt.getText());
				Room room=roomDao.search(roomId);
				if (room==null) {
					JOptionPane.showMessageDialog(null, "查无此会议室！");
				}else if (!room.getStatus().equals("空闲")) {
					JOptionPane.showMessageDialog(null, "该会议室已被预定，请换一个！");
				}else {
//					添加预约记录
					String sql="INSERT INTO `order` VALUES(0,?,?,'审核中')";
					try {
						int i=DataUtils.Update(sql, new Object[] {id,roomId});
						if (i>0) {
							JOptionPane.showMessageDialog(null, "预约成功");
							showMine(id);
						}
						else {
							JOptionPane.showMessageDialog(null, "预约失败！");
						}
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				
			}
		});
		jb.setFont(new Font("宋体", Font.PLAIN, 16));
		jb.setBounds(279, 56, 135, 21);
		add(jb);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 125, 425, 288);
		add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JLabel lblNewLabel_1 = new JLabel("\u6211\u7684\u9884\u7EA6\u4E00\u89C8\uFF1A");
		lblNewLabel_1.setFont(new Font("宋体", Font.PLAIN, 16));
		lblNewLabel_1.setBounds(10, 101, 233, 21);
		add(lblNewLabel_1);
		showMine(id);
	}
//	显示我的预约情况
	public void showMine(int id) {
		String sql="SELECT\r\n"
				+ "	r.`name`,\r\n"
				+ "	r.address,\r\n"
				+ "	r.seat_num,\r\n"
				+ "	o.`status` \r\n"
				+ "FROM\r\n"
				+ "	`user` u\r\n"
				+ "	LEFT JOIN `order` o ON u.id = o.user_id\r\n"
				+ "	LEFT JOIN room r ON r.id = o.room_id \r\n"
				+ "WHERE\r\n"
				+ "	u.id ="+String.valueOf(id)+";";
		try {
			TableModel model=new DefaultTableModel(DataUtils.getSetArrays(DataUtils.query(sql, new Object[] {})),header);
			table.setModel(model);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
